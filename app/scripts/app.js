'use strict';
var blumaApp = angular
    .module('blumaApp', ['ngRoute', 'ngCookies', 'ngResource', 'angularMoment', 'formly', 'ui.select']);

blumaApp.run(function($rootScope, $location, $cookies, $http) {
    $rootScope.$on("$routeChangeStart", function(event, next) {

        $rootScope.currentUser = $cookies.getObject('currentUser');
        if ($rootScope.currentUser == null) {
            // no logged user, redirect to /login
            if (next.templateUrl === "views/login.html") {
                $location.path("/login");
            } else if (next.templateUrl === "views/signup.html") {
                $location.path("/signup");
            } else if (next.templateUrl === "views/resetpassword.html") {
                $location.path("/resetpassword");
            } else {
                $location.path("/login");
            }
        } else {
            $rootScope.user = $cookies.getObject('currentUser');
            if (next.templateUrl === "views/login.html") {
                $location.path("/");
            } else if (next.templateUrl === "views/signup.html") {
                $location.path("/");
            } else if (next.templateUrl === "views/requestreset.html") {
                $location.path("/");
            } else if (next.templateUrl === "views/resetpassword.html") {
                $location.path("/");
            }            
        }
    });    

    $rootScope.baseUrl = 'https://xxxx.azurewebsites.net/api/';
    //$rootScope.baseUrl = 'http://localhost:56130/api/';

    $rootScope.pageSize = 20;

    $http.defaults.headers.common['Authorization'] = "Bearer " + $cookies.get('token');
});

// configure our routes
blumaApp.config(function($routeProvider) {
    $routeProvider

    // route for the home page
        .when('/', {
        templateUrl: 'views/dashboard.html',
        controller: 'DashboardCtrl',
        controllerAs: 'dashboard'
    })

    .when('/login', {
            templateUrl: 'views/login.html',
            controller: 'LoginCtrl',
            controllerAs: 'login'
        })
        .when('/signup', {
            templateUrl: 'views/signup.html',
            controller: 'SignupCtrl',
            controllerAs: 'signup'
        })
        .when('/resetpassword', {
            templateUrl: 'views/resetpassword.html',
            controller: 'ResetpasswordCtrl',
            controllerAs: 'resetpassword'
        })
        
        .when('/profile', {
            templateUrl: 'views/profile.html',
            controller: 'ProfileCtrl',
            controllerAs: 'profile'
        })
        
        .when('/reports', {
            templateUrl: 'views/reports.html',
            controller: 'ReportsCtrl',
            controllerAs: 'reports'
        })        
        .when('/newpage', {
          templateUrl: 'views/newpage.html',
          controller: 'NewpageCtrl',
          controllerAs: 'newpage'
        })
        .when('/anotherpage', {
          templateUrl: 'views/anotherpage.html',
          controller: 'AnotherpageCtrl',
          controllerAs: 'anotherpage'
        })
        .otherwise({
            redirectTo: "/"
        });
});

blumaApp.config(function(formlyConfigProvider) {
    formlyConfigProvider.setType({
        name: 'input',
        template: '<div class="field"><label>{{options.label}}</label><input type="text" ng-model="model[options.key]"></div>'
    });
});
