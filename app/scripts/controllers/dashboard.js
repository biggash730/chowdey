'use strict';
/**
 * @ngdoc function
 * @name bulkCreditApp.controller:DashboardCtrl
 * @description
 * # DashboardCtrl
 * Controller of the bulkCreditApp
 */
angular.module('blumaApp').controller('DashboardCtrl', function($scope, $cookies, $location, $rootScope, $http) {
    $rootScope.page = "Dashboard";
    var vm = $scope;
    vm.SelectedMonth = "";
    vm.transactions = [];
    vm.TInputs = 0;
    vm.TOutputs = 0;
    vm.TDifference = 0;
    vm.date = new Date();
    $("#monthDropdown").dropdown({
        onChange: function(val) {
            console.log(val);
            vm.SelectedMonth = val;
            vm.GetTransbyMonth();
        }
    });
    vm.GetTransbyMonth = function() {
        NProgress.start();
        $http.get($rootScope.baseUrl + 'dashboard/transbymonth?month=' + vm.SelectedMonth).then(function(res) {
            NProgress.done();
            if (res.data.success) {
                var chart = c3.generate({
                    bindto: '#chart1',
                    data: {
                        columns: [
                            ['Inputs', res.data.data.Inputs],
                            ['Outputs', res.data.data.Outputs]
                        ],
                        type: 'donut',
                        colors: {
                            Inputs: '#d95c5c',
                            Outputs: '#564f8a'
                        }
                    }
                });
            } else {
                toastr.error(res.data.message);
            }
        }, function(res) {
            NProgress.done();
        });
    };
    vm.GetTransbyMonth();
    vm.GetTransactions = function() {
        NProgress.start();
        $http.get($rootScope.baseUrl + 'dashboard/transactions').then(function(res) {
            NProgress.done();
            if (res.data.success) {
                vm.transactions = res.data.data;
            } else {
                toastr.error(res.data.message);
            }
        }, function(res) {
            NProgress.done();
        });
    };
    vm.GetTransactions();
    vm.GetTotalTransactions = function() {
        NProgress.start();
        $http.get($rootScope.baseUrl + 'dashboard/totaltransactions').then(function(res) {
            NProgress.done();
            if (res.data.success) {
                vm.TInputs = res.data.data.Inputs;
                vm.TOutputs = res.data.data.Outputs;
                vm.TDifference = res.data.data.Inputs - res.data.data.Outputs;
            } else {
                toastr.error(res.data.message);
            }
        }, function(res) {
            NProgress.done();
        });
    };
    vm.GetTotalTransactions();
});