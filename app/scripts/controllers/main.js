'use strict';
angular.module('blumaApp')
    .controller('MainCtrl', function($scope, $location, $cookies, $rootScope) {
        var vm = $scope;
        $("#userDropdown").dropdown({
            onChange: function(val) {
                if (val == 'logout') {
                    vm.logoff();
                }
            }
        });

        Waves.attach('button', ['waves-button', 'waves-float', 'waves-light']);
        Waves.attach('.button', ['waves-button', 'waves-float', 'waves-light']);
        Waves.attach('.card', ['waves-float', 'waves-light']);
        Waves.attach('.message', ['waves-float', 'waves-light']);
        Waves.init();

        $(document)
            .ready(function() {
                $rootScope.validationRules = {
                    name: {
                        identifier: 'name',
                        rules: [{
                            type: 'empty',
                            prompt: 'Please enter the name'
                        }]
                    },
                    fullname: {
                        identifier: 'fullname',
                        rules: [{
                            type: 'empty',
                            prompt: 'Please enter the full name'
                        }]
                    },
                    username: {
                        identifier: 'username',
                        rules: [{
                            type: 'empty',
                            prompt: 'Please enter a username'
                        }]
                    },
                    password: {
                        identifier: 'password',
                        rules: [{
                            type: 'empty',
                            prompt: 'Please enter a password'
                        }, {
                            type: 'length[6]',
                            prompt: 'Your password must be at least 6 characters'
                        }]
                    },
                    email: {
                        identifier: 'email',
                        rules: [{
                            type: 'email',
                            prompt: 'The email must be valid'
                        }]
                    },
                    phonenumber1: {
                        identifier: 'phonenumber1',
                        rules: [{
                            type: 'empty',
                            prompt: 'Please enter a mobile number'
                        }]
                    },
                    phonenumber: {
                        identifier: 'phonenumber',
                        rules: [{
                            type: 'empty',
                            prompt: 'Please enter a phone number'
                        }]
                    },
                    opassword: {
                        identifier: 'opassword',
                        rules: [{
                            type: 'empty',
                            prompt: 'Please enter a password'
                        }, {
                            type: 'length[6]',
                            prompt: 'Your password must be at least 6 characters'
                        }]
                    },
                    npassword: {
                        identifier: 'npassword',
                        rules: [{
                            type: 'empty',
                            prompt: 'Please enter a password'
                        }, {
                            type: 'length[6]',
                            prompt: 'Your password must be at least 6 characters'
                        }]
                    },
                    cpassword: {
                        identifier: 'cpassword',
                        rules: [{
                            type: 'empty',
                            prompt: 'Please enter a password'
                        }, {
                            type: 'length[6]',
                            prompt: 'Your password must be at least 6 characters'
                        }]
                    },
                    accountnumber: {
                        identifier: 'accountnumber',
                        rules: [{
                            type: 'empty',
                            prompt: 'Please enter an account number'
                        }]
                    },
                    title: {
                        identifier: 'title',
                        rules: [{
                            type: 'empty',
                            prompt: 'Please enter a text here'
                        }]
                    },
                    amount: {
                        identifier: 'amount',
                        rules: [{
                            type: 'empty',
                            prompt: 'Please enter an amount'
                        }]
                    },
                    date: {
                        identifier: 'date',
                        rules: [{
                            type: 'empty',
                            prompt: 'Please select a date'
                        }]
                    },
                    answer: {
                        identifier: 'answer',
                        rules: [{
                            type: 'empty',
                            prompt: 'Please enter an answer'
                        }]
                    },
                    notes: {
                        identifier: 'notes',
                        rules: [{
                            type: 'empty',
                            prompt: 'Please enter a note'
                        }]
                    }


                };
            });

        vm.logoff = function() {
            //logout on the server
            $cookies.remove('currentUser', []);
            toastr.success('Logged Out');
            $location.path("/login");
        };

        vm.menuIconClick = function() {
            $('#smallSideMenu')
  .sidebar('toggle')
;
        };

        $rootScope.$on('openreport', function(event, args) {
            $rootScope.REPORT = args.report;
        });

    });
