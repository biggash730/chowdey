'use strict';

/**
 * @ngdoc function
 * @name bulkCreditApp.controller:ResetpasswordCtrl
 * @description
 * # ResetpasswordCtrl
 * Controller of the bulkCreditApp
 */
angular.module('blumaApp')
  .controller('ResetpasswordCtrl', function($scope, $rootScope, $http) {
        $rootScope.page = "Reset";
        var vm = $scope;        
        vm.user = {};

        vm.doReset = function() {
            //todo: Validate the object here
            NProgress.start();
            $http.post($rootScope.baseUrl + 'Account/resetpassword?email='+vm.user.UserName).then(function(res) {
                NProgress.done();                
                if (res.data.success) {
                    vm.user = {};
                    toastr.success(res.data.message);
                    $location.path("/login");
                }
                else{
                    toastr.error(res.data.message);
                } 
            }, function(res) {
                console.dir(res);
                toastr.error('Reset Failed');
                NProgress.done();
            });
        };
    });
