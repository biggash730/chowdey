'use strict';
angular.module('blumaApp').controller('SignupCtrl', function($scope, $cookies, $location, $rootScope, $http) {
    $rootScope.page = "Minuti";
    var vm = $scope;
    vm.user = {};
    
    vm.register = function() {
        var err = '';
        if (!vm.user.FullName) err = err + 'Please enter a full name \n';
        if (!vm.user.UserName) err = err + 'Please enter a username \n';
        if (!vm.user.CompanyName) err = err + 'Please enter a company name \n';
        if (!vm.user.OldTin) err = err + 'Please enter your old TIN \n';
        if (err != '') {
            toastr.error(err);
            return;
        } else {
            NProgress.start();
            $http.post($rootScope.baseUrl + 'Account/createuser', vm.user).then(function(res) {
                NProgress.done();
                if (res.data.success) {
                    toastr.success('Please check your email for your password');
                    $location.path("/login");
                } else {
                    toastr.error(res.data.message);
                }
            }, function(res) {
                console.log(res);
                toastr.error('Registration Failed');
                NProgress.done();
            });
        }
    };
});