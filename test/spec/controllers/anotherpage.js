'use strict';

describe('Controller: AnotherpageCtrl', function () {

  // load the controller's module
  beforeEach(module('blumaApp'));

  var AnotherpageCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AnotherpageCtrl = $controller('AnotherpageCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(AnotherpageCtrl.awesomeThings.length).toBe(3);
  });
});
