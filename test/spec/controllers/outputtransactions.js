'use strict';

describe('Controller: OutputtransactionsCtrl', function () {

  // load the controller's module
  beforeEach(module('bulkCreditApp'));

  var OutputtransactionsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    OutputtransactionsCtrl = $controller('OutputtransactionsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(OutputtransactionsCtrl.awesomeThings.length).toBe(3);
  });
});
